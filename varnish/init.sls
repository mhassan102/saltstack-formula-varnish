{% from "varnish/map.jinja" import varnish with context %}


varnish:
  pkg:
    - installed
    - name: {{ varnish.pkg }}
    {% if varnish.version is defined %}
    - version: {{ varnish.version }}
    {% endif %}
