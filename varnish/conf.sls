{% from "varnish/map.jinja" import varnish with context %}


include:
  - varnish


{% set files_switch = salt['pillar.get']('varnish:files_switch', ['id']) %}

{{ varnish.config }}:
  file:
    - managed
    - source:
      - salt://varnish/files/default/etc/default/varnish.jinja
      - salt://varnish/files/default/etc/default/varnish.jinja
    - template: jinja
    - require:
      - pkg: varnish
    - require_in:
      - service: varnish

# Below we deploy the vcl files and we trigger a reload of varnish
{% for file in salt['pillar.get']('varnish:vcl:files', ['default.vcl']) %}
/etc/varnish/{{ file }}:
  file:
    - managed
    - makedirs: true
    - source:
      - salt://varnish/files/default/etc/varnish/{{ file }}
      - salt://varnish/files/default/etc/varnish/{{ file }}.jinja
    - template: jinja
    - require:
      - pkg: varnish
    - watch_in:
      - service: varnish
{% endfor %}
