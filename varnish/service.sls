
{% from "varnish/map.jinja" import varnish with context %}


varnish-service:
  service:
    - running
    - name: {{ varnish.service }}
    - enable: True
    - reload: True
    - require:
      - pkg: varnish

start_service:
   cmd.run:
      - name: 'service varnish restart'
      - user: root
      - group: root

